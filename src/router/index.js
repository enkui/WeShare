/* eslint-disable space-before-function-paren */
/* eslint-disable indent */
// eslint-disable-next-line semi
import Vue from 'vue';
// eslint-disable-next-line semi
import VueRouter from 'vue-router';
import rate from './../components/resource/rate.vue'
import uploaded from './../components/resource/uploaded.vue'
import favorite from './../components/resource/favorite.vue'
import help from './../components/resource/help.vue'

Vue.use(VueRouter)

const routes = [{
  // 首页
  path: '/',
  name: 'Home',
  component: () => import('@/views/Home.vue')
},
{
  // 资源管理
  path: '/resource',
  name: 'Resource',
  component: () => import('@/views/Resource.vue'),
  children: [{
    path: '/rate',
    component: rate,
    name: 'rate'
  },
  {
    path: '/uploaded',
    component: uploaded,
    name: 'uploaded'
  },
  {
    path: '/favorite',
    component: favorite,
    name: 'favorite'
  },
  {
    path: '/help',
    component: help,
    name: 'help'
  }
  ]
},
{
  // 这是注册的路径
  path: '/Register',
  name: 'Register',
  component: () => import('@/views/Register.vue')
},
{
  // 这是登录的路径
  path: '/login',
  name: 'Login',
  component: () => import('@/views/Login.vue')
},
{
  // 这是个人中心的路径
  path: '/my',
  name: 'My',
  component: () => import('@/views/My.vue'),
  // 二级路由 子路由的设置
  children: [{
    path: '/My',
    redirect: '/profile'
  },
  // 个人信息

  {
    path: '/profile',
    component: () => import('@/views/Profile.vue')
  },
  //  系统设置
  {
    path: '/system',
    component: () => import('@/views/System.vue')
  },
  // 个人信息修改
  {
    path: '/profile-edit',
    component: () => import('@/views/Profile-edit.vue')
  },
  {
    // 消息管理
    path: '/message',
    name: 'message',
    component: () => import('@/views/Message.vue')
  }
  ]
},
{
  // 资源列表
  path: '/search/:sort1id/:keywords',
  name: 'Search',
  component: () => import('@/views/Search.vue')
},
{
  // 资源详情
  path: '/sharedetail/:shareId',
  name: 'ShareDetail',
  component: () => import('@/views/ShareDetail.vue')
},
{
  // 求助列表
  path: '/helplist',
  name: 'HelpList',
  component: () => import('@/views/HelpList.vue')
},
{
  // 求助详情
  path: '/helpdetail/:askId',
  name: 'HelpDetail',
  component: () => import('@/views/HelpDetail.vue')
},
{
  // 分享资源
  path: '/shareresource',
  name: 'ShareResource',
  component: () => import('@/views/ShareResource.vue')
},
{
  // 发布求助
  path: '/sendhelp',
  name: 'SendHelp',
  component: () => import('@/views/SendHelp.vue')

}

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
// 添加路由守卫  beforeEach导航守卫
// to  表示将要访问你的路径 form 从哪来  next 是下一个要进行的操作
router.beforeEach((to, from, next) => {
  console.log(to.path.substr(0, 7))
  // 对用户访问的路由进行拦截， 如果访问的是登录页 那么这是就直接放行
  if (to.path === '/login' || to.path === '/' || to.path === '/Register' || to.path.substr(0, 7) === '/search' || to.path === '/helplist') {
    return next() // 放行
  }
  // 如果访问的不是登录页 则我们需要来检测用户是否登陆 如果登陆 则直接放行  否则 就跳转到登录页
  const token = window.sessionStorage.getItem('token')
  if (!token) { // 没有koken
    return next('/login')
  }
  next()
})

const VueRouterPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (to) {
  return VueRouterPush.call(this, to).catch(err => err)
}

export default router
