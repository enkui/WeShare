import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/element.js'
import './assets/css/global.css'
import headera from '@/components/Header.vue'
import footera from '@/components/Footer.vue'

import ElementUI from 'element-ui'
import axios from 'axios'
import editpassword from '@/components/system/editpassword.vue'
import about from '@/components/system/about.vue'
import help from '@/components/system/help.vue'
import answer from '@/components/information/Myanswer.vue'
import myask from '@/components/information/Myask.vue'
import mycomment from '@/components/information/mycomment.vue'
import system from '@/components/information/System.vue'

Vue.config.productionTip = false
axios.default.baseURL = 'http://120.26.179.27:8890/'
// axios.interceptors.request.use(config => {
//   config.headers.Authorization = window.sessionStorage.getItem('token')
//   return config
// })
Vue.prototype.$http = axios

Vue.use(ElementUI)
Vue.config.productionTip = false

Vue.component('headera', headera)
Vue.component('footera', footera)

Vue.component('editpassword', editpassword)
Vue.component('about', about)
Vue.component('help', help)
Vue.component('myanswer', answer)
Vue.component('myask', myask)
Vue.component('mycomment', mycomment)
Vue.component('system', system)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
